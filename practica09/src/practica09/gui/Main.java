package practica09.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.swt.SWTResourceManager;

import practica09.op.BufferLimitado;
import practica09.op.Consumidor;
import practica09.op.Productor;

import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class Main {

	protected Shell shell;
	public static Display display;
	public static Canvas semaforo_1;
	public static Canvas semaforo_2;
	public static Canvas buffer;
	public static Canvas semaforo_3;
	private Label lbl_consumir;
	private Label lbl_producir;
	private Spinner spin_prod;
	private Spinner spin_cons;
	private Label lblTiempoDe;
	private Label lblmilisegundos;
	private Spinner spin_tProd;
	private Label lblTiempoDeConsumo;
	private Label lblmilisegundos_1;
	private Spinner spin_tCons;
	private Button btnAceptar;
	private Label label_1;
	private Label lblProcesoConsumidor;
	private Label lblProcesoProductor;
	private Label label_2;
	private Label label_3;
	private static Label tuboC_1;
	private static Label tuboC_2;
	private static Label tuboC_3;
	private static Label tuboP_1;
	private static Label tuboP_2;
	private static Label tuboP;
	
	private final static int t_buffer = 10;
	private static int ocupados = 0;
	private static int desocupados = 0;
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Main window = new Main();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		display = Display.getCurrent();
		shell = new Shell(display, SWT.CLOSE | SWT.TITLE | SWT.MIN);
		shell.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		shell.setMinimumSize(new Point(1000, 720));
		shell.setMaximized(false);
		shell.setSize(1000, 720);
		shell.setText("Semáforos");

		Label lbl_1 = new Label(shell, SWT.NONE);
		lbl_1.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lbl_1.setBackground(SWTResourceManager.getColor(135, 206, 250));
		lbl_1.setBounds(375, 165, 125, 125);
		
		Label lbl_2 = new Label(shell, SWT.NONE);
		lbl_2.setBackground(SWTResourceManager.getColor(147, 112, 219));
		lbl_2.setBounds(738, 165, 125, 125);
		
		Label lblBuffer = new Label(shell, SWT.NONE);
		lblBuffer.setFont(SWTResourceManager.getFont("Arial", 15, SWT.NORMAL));
		lblBuffer.setBounds(335, 414, 175, 30);
		lblBuffer.setText("Buffer");
		
		semaforo_1 = new Canvas(shell, SWT.BORDER | SWT.NO_REDRAW_RESIZE);
		semaforo_1.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
	            e.gc.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
	            e.gc.fillOval(0,0,46,46);
	            
	            e.gc.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
	            e.gc.fillOval(0,50,46,46);
			}
		});
		semaforo_1.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		semaforo_1.setBounds(510, 175, 50, 100);
		
		semaforo_2 = new Canvas(shell, SWT.BORDER | SWT.NO_REDRAW_RESIZE);
		semaforo_2.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e1) {
				e1.gc.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
	            e1.gc.fillOval(0,0,46,46);
	            
	            e1.gc.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
	            e1.gc.fillOval(0,50,46,46);
			}
		});
		semaforo_2.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		semaforo_2.setBounds(682, 175, 50, 100);
		
		buffer = new Canvas(shell, SWT.BORDER | SWT.NO_REDRAW_RESIZE);
		buffer.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent b) {
				for(int i = 0; i < 10; i++) {
					b.gc.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
					b.gc.fillRectangle((56 * i), 0, 55, 48);
				}
			}
		});
		buffer.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		buffer.setBounds(335, 450, 560, 50);
		
		semaforo_3 = new Canvas(shell, SWT.BORDER | SWT.NO_REDRAW_RESIZE);
		semaforo_3.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e2) {
				e2.gc.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
	            e2.gc.fillOval(0,0,46,46);
	            
	            e2.gc.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
	            e2.gc.fillOval(0,50,46,46);
			}
		});
		semaforo_3.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		semaforo_3.setBounds(594, 535, 50, 100);
		
		CLabel barra_izq = new CLabel(shell, SWT.NONE);
		barra_izq.setBackground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		barra_izq.setBounds(280, 75, 3, 575);
		barra_izq.setText("");
		
		label_1 = new Label(shell, SWT.NONE);
		label_1.setBackground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		label_1.setBounds(280, 75, 692, 3);
		
		label_2 = new Label(shell, SWT.NONE);
		label_2.setBackground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		label_2.setBounds(280, 647, 692, 3);
		
		label_3 = new Label(shell, SWT.NONE);
		label_3.setBackground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		label_3.setBounds(970, 75, 3, 575);
		
		Label lbl_titulo = new Label(shell, SWT.NONE);
		lbl_titulo.setAlignment(SWT.CENTER);
		lbl_titulo.setFont(SWTResourceManager.getFont("Arial", 24, SWT.NORMAL));
		lbl_titulo.setBounds(10, 10, 962, 45);
		lbl_titulo.setText("El productor y el consumidor");
		
		lbl_consumir = new Label(shell, SWT.NONE);
		lbl_consumir.setFont(SWTResourceManager.getFont("Arial", 15, SWT.NORMAL));
		lbl_consumir.setBounds(15, 75, 250, 30);
		lbl_consumir.setText("Artículos a consumir");
		
		spin_cons = new Spinner(shell, SWT.BORDER);
		spin_cons.setMinimum(1);
		spin_cons.setSelection(1);
		spin_cons.setFont(SWTResourceManager.getFont("Arial", 15, SWT.NORMAL));
		spin_cons.setBounds(14, 110, 250, 35);
		
		lbl_producir = new Label(shell, SWT.NONE);
		lbl_producir.setFont(SWTResourceManager.getFont("Arial", 15, SWT.NORMAL));
		lbl_producir.setBounds(15, 175, 250, 30);
		lbl_producir.setText("Artículos a producir");
		
		spin_prod = new Spinner(shell, SWT.BORDER);
		spin_prod.setMinimum(1);
		spin_prod.setSelection(1);
		spin_prod.setFont(SWTResourceManager.getFont("Arial", 15, SWT.NORMAL));
		spin_prod.setBounds(15, 211, 250, 35);
		
		Label label = new Label(shell, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setBounds(10, 60, 962, 2);
		
		lblTiempoDe = new Label(shell, SWT.NONE);
		lblTiempoDe.setFont(SWTResourceManager.getFont("Arial", 15, SWT.NORMAL));
		lblTiempoDe.setBounds(15, 285, 250, 30);
		lblTiempoDe.setText("Tiempo de producción");
		
		lblmilisegundos = new Label(shell, SWT.NONE);
		lblmilisegundos.setFont(SWTResourceManager.getFont("Arial", 15, SWT.NORMAL));
		lblmilisegundos.setBounds(15, 320, 250, 30);
		lblmilisegundos.setText("(Milisegundos)");
		
		spin_tProd = new Spinner(shell, SWT.BORDER);
		spin_tProd.setPageIncrement(1);
		spin_tProd.setIncrement(10);
		spin_tProd.setMaximum(100000);
		spin_tProd.setMinimum(10);
		spin_tProd.setSelection(10);
		spin_tProd.setFont(SWTResourceManager.getFont("Arial", 15, SWT.NORMAL));
		spin_tProd.setBounds(15, 360, 250, 35);
		
		lblTiempoDeConsumo = new Label(shell, SWT.NONE);
		lblTiempoDeConsumo.setFont(SWTResourceManager.getFont("Arial", 15, SWT.NORMAL));
		lblTiempoDeConsumo.setBounds(15, 430, 250, 30);
		lblTiempoDeConsumo.setText("Tiempo de consumo");
		
		lblmilisegundos_1 = new Label(shell, SWT.NONE);
		lblmilisegundos_1.setFont(SWTResourceManager.getFont("Arial", 15, SWT.NORMAL));
		lblmilisegundos_1.setBounds(15, 460, 250, 30);
		lblmilisegundos_1.setText("(Milisegundos)");
		
		spin_tCons = new Spinner(shell, SWT.BORDER);
		spin_tCons.setMaximum(100000);
		spin_tCons.setMinimum(10);
		spin_tCons.setSelection(10);
		spin_tCons.setIncrement(10);
		spin_tCons.setFont(SWTResourceManager.getFont("Arial", 15, SWT.NORMAL));
		spin_tCons.setBounds(15, 500, 250, 35);
		
		lblProcesoConsumidor = new Label(shell, SWT.NONE);
		lblProcesoConsumidor.setAlignment(SWT.CENTER);
		lblProcesoConsumidor.setFont(SWTResourceManager.getFont("Arial", 15, SWT.NORMAL));
		lblProcesoConsumidor.setBounds(360, 100, 150, 60);
		lblProcesoConsumidor.setText("Proceso\r\nconsumidor");
		
		lblProcesoProductor = new Label(shell, SWT.NONE);
		lblProcesoProductor.setFont(SWTResourceManager.getFont("Arial", 15, SWT.NORMAL));
		lblProcesoProductor.setAlignment(SWT.CENTER);
		lblProcesoProductor.setBounds(740, 100, 150, 60);
		lblProcesoProductor.setText("Proceso\r\nproductor");
		
		tuboC_1 = new Label(shell, SWT.NONE);
		tuboC_1.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BORDER));
		tuboC_1.setBounds(300, 235, 3, 352);
		
		tuboC_2 = new Label(shell, SWT.NONE);
		tuboC_2.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BORDER));
		tuboC_2.setBounds(300, 235, 70, 3);
		
		tuboC_3 = new Label(shell, SWT.NONE);
		tuboC_3.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BORDER));
		tuboC_3.setBounds(300, 584, 288, 3);
		
		tuboP_1 = new Label(shell, SWT.NONE);
		tuboP_1.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BORDER));
		tuboP_1.setBounds(650, 584, 288, 3);
		
		tuboP_2 = new Label(shell, SWT.NONE);
		tuboP_2.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BORDER));
		tuboP_2.setBounds(934, 235, 3, 352);
		
		tuboP = new Label(shell, SWT.NONE);
		tuboP.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BORDER));
		tuboP.setBounds(868, 235, 70, 3);
		
		btnAceptar = new Button(shell, SWT.NONE);
		btnAceptar.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ocupados = 0;
				desocupados = 0;
				
				int num_consumos = spin_cons.getSelection();
				int num_productos = spin_prod.getSelection();
				int t_produccion = spin_tProd.getSelection();
				int t_consumo = spin_tCons.getSelection();
				
				if(num_consumos > num_productos) 
				{
					MessageBox diag = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
					diag.setMessage("La cantidad de artículos a consumir debe ser menor o igual que la de articulos a producir.");
					diag.open();
					return;
				}
				
				if(num_productos-num_consumos > 10) 
				{
					MessageBox diag = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
					diag.setMessage("La cantidad de artículos a producir no debe sobrepasar por más de 10 a la de consumir.");
					diag.open();
					return;
				}
				
				BufferLimitado buffer = new BufferLimitado();
		    	Productor productor = new Productor(buffer, num_productos, t_produccion);
		    	Consumidor consumidor = new Consumidor(buffer, num_consumos, t_consumo);
		    }
		});
		btnAceptar.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		btnAceptar.setFont(SWTResourceManager.getFont("Arial", 15, SWT.BOLD));
		btnAceptar.setBounds(15, 575, 250, 45);
		btnAceptar.setText("Aceptar");
		btnAceptar.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_CYAN));
		btnAceptar.setBounds(15, 575, 250, 45);
		btnAceptar.setText("Aceptar");
		btnAceptar.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_CYAN));

	}
	
	
	public static void encenderSemaforo_1() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				semaforo_1.addPaintListener(new PaintListener() {
					public void paintControl(PaintEvent e) {
			            e.gc.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
			            e.gc.fillOval(0,0,46,46);
			            
			            e.gc.setBackground(display.getSystemColor(SWT.COLOR_GREEN));
			            e.gc.fillOval(0,50,46,46);
					}
				});
				
				semaforo_1.redraw();
				semaforo_1.update();
			}
		});
		
	}
	
	public static void apagarSemaforo_1() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				semaforo_1.addPaintListener(new PaintListener() {
					public void paintControl(PaintEvent e) {
			            e.gc.setBackground(display.getSystemColor(SWT.COLOR_RED));
			            e.gc.fillOval(0,0,46,46);
			            
			            e.gc.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
			            e.gc.fillOval(0,50,46,46);
					}
				});
				
				semaforo_1.redraw();
				semaforo_1.update();
			}
		});

	}

	public static void encenderSemaforo_2() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				semaforo_2.addPaintListener(new PaintListener() {
					public void paintControl(PaintEvent e2) {
			            e2.gc.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
			            e2.gc.fillOval(0,0,46,46);
			            
			            e2.gc.setBackground(display.getSystemColor(SWT.COLOR_GREEN));
			            e2.gc.fillOval(0,50,46,46);
					}
				});
				
				semaforo_2.redraw();
				semaforo_2.update();
			}
		});

	}
	
	public static void apagarSemaforo_2() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				semaforo_2.addPaintListener(new PaintListener() {
					public void paintControl(PaintEvent e2) {
			            e2.gc.setBackground(display.getSystemColor(SWT.COLOR_RED));
			            e2.gc.fillOval(0,0,46,46);
			            
			            e2.gc.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
			            e2.gc.fillOval(0,50,46,46);
					}
				});
				
				semaforo_2.redraw();
				semaforo_2.update();
			}
		});

	}
	
	public static void encenderSemaforo_3() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				semaforo_3.addPaintListener(new PaintListener() {
					public void paintControl(PaintEvent e3) {
			            e3.gc.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
			            e3.gc.fillOval(0,0,46,46);
			            
			            e3.gc.setBackground(display.getSystemColor(SWT.COLOR_GREEN));
			            e3.gc.fillOval(0,50,46,46);
					}
				});
				
				semaforo_3.redraw();
				semaforo_3.update();
			}
		});

	}
	
	public static void apagarSemaforo_3() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				semaforo_3.addPaintListener(new PaintListener() {
					public void paintControl(PaintEvent e3) {
			            e3.gc.setBackground(display.getSystemColor(SWT.COLOR_RED));
			            e3.gc.fillOval(0,0,46,46);
			            
			            e3.gc.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
			            e3.gc.fillOval(0,50,46,46);
					}
				});

				semaforo_3.redraw();
				semaforo_3.update();
			}
		});

	}
	
	public static void bloquearProceso_P() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				tuboP.setBackground(display.getSystemColor(SWT.COLOR_RED));
				tuboP_1.setBackground(display.getSystemColor(SWT.COLOR_RED));
				tuboP_2.setBackground(display.getSystemColor(SWT.COLOR_RED));
			}
		});

	}

	public static void desbloquearProceso_P() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				tuboP.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
				tuboP_1.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
				tuboP_2.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
			}
		});

	}
	
	public static void bloquearProceso_C() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				tuboC_1.setBackground(display.getSystemColor(SWT.COLOR_RED));
				tuboC_2.setBackground(display.getSystemColor(SWT.COLOR_RED));
				tuboC_3.setBackground(display.getSystemColor(SWT.COLOR_RED));
			}
		});
		
	}
	
	public static void desbloquearProceso_C() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				tuboC_1.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
				tuboC_2.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
				tuboC_3.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
			}
		});

	}
	
	public static void llenarCeldaBuffer() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				if(ocupados >= t_buffer)
					return;
					
				ocupados++;
				desocupados--;
				
				buffer.addPaintListener(new PaintListener() {
					public void paintControl(PaintEvent b) 
					{
						for(int i = 0; i < ocupados; i++) {
							b.gc.setBackground(display.getSystemColor(SWT.COLOR_DARK_CYAN));
							b.gc.fillRectangle((56 * i), 0, 55, 48);
						}
					}
				});
				buffer.redraw();
				buffer.update();
			}
		});

	}
	
	public static void quitarCeldaBuffer() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				if(desocupados >= t_buffer)
					return;
				
				desocupados++;
				ocupados--;
				
				buffer.addPaintListener(new PaintListener() {
					public void paintControl(PaintEvent b) 
					{
						for(int i = 9; i == desocupados; i--) {
							System.out.println(i);
							b.gc.setBackground(display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
							b.gc.fillRectangle((56 * i), 0, 55, 48);
						}
					}
				});
				buffer.redraw();
				buffer.update();
			}
		});

	}
	
}
