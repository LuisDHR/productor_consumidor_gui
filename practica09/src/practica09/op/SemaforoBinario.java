package practica09.op;

import practica09.gui.Main;

public class SemaforoBinario 
{
	boolean value;
	
	SemaforoBinario( boolean initValue )
	{
		value = initValue;
	}
	
	public synchronized void P()
	{
		while( value == false )
		{
			Util.myWait(this); //en cola de procesos bloqueados
		}
		// Main.apagarSemaforo_3();
		Main.encenderSemaforo_3();
		value = false;
	}
	
	public synchronized void V()
	{
		// Main.encenderSemaforo_3();
		Main.apagarSemaforo_3();
		value = true;
		notify();
	}

}
