package practica09.op;

public class SemaforoContador 
{
	int value;
	
	public SemaforoContador( int initValue )
	{
		value = initValue;
	}
	
	public synchronized boolean P() 
	{
		value--;
		if( value < 0 )
		{
			Util.myWait( this );
			return false;
		}
		return true;
	}

	public synchronized void V() 
	{
		value++;
		if( value <= 0 )
		{
			notify();
		}
	}

}
