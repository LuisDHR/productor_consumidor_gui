package practica09.op;

import practica09.gui.Main;

public class Consumidor implements Runnable 
{
	BufferLimitado b = null;
	int num_consumos = 0;
	int t_consumo = 0;
	
	public Consumidor(BufferLimitado initb, int n_consumos, int t_cons) 
	{
		b = initb;
		num_consumos = n_consumos;
		t_consumo = t_cons;
		new Thread( this ).start();
	}
	
	@Override
	public void run() 
	{
		double item;
		int i = 0;
		Main.encenderSemaforo_1();
		while( i < num_consumos ) 
		{
			item = b.fetch();
			Main.quitarCeldaBuffer();
			System.out.println( "Artículo extraído " + item );
			Util.mySleep(t_consumo);
			i++;
		}
		Main.apagarSemaforo_1();
		Main.desbloquearProceso_C();
		Main.desbloquearProceso_P();
		Main.apagarSemaforo_3();
	}
}
