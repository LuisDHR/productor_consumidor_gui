package practica09.op;

import java.util.Random;

import practica09.gui.Main;

public class Productor implements Runnable 
{
	BufferLimitado b = null;
	int num_productos = 0;
	int t_produccion = 0;
	
	public Productor(BufferLimitado initb, int n_productos, int t_produc)
	{
		b = initb;
		num_productos = n_productos;
		t_produccion = t_produc;
		
		new Thread( this ).start();
	}

	@Override
	public void run() 
	{
		double item;
		Random r = new Random();
		int i = 0;
		Main.encenderSemaforo_2();
		while( i < num_productos )
		{ 
			item = r.nextDouble();
			System.out.println( "\nArtículo producido " + item );
			b.deposit( item );
			Util.mySleep(t_produccion);
			Main.llenarCeldaBuffer();
			i++;
		}
		Main.apagarSemaforo_2();
		Main.desbloquearProceso_C();
		Main.desbloquearProceso_P();
		Main.apagarSemaforo_3();
	}

}
