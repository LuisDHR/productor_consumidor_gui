package practica09.op;

import practica09.gui.Main;

public class BufferLimitado 
{
	final int size = 10;
	double buffer[] = new double[size];
	int inBuf = 0; 
	int outBuf = 0;
	
	SemaforoBinario mutex = new SemaforoBinario(true);
	SemaforoContador isEmpty = new SemaforoContador(0);
	SemaforoContador isFull = new SemaforoContador( size );
	
	public void deposit( double value )
	{
		if(!isFull.P()) // espera si el buffer está lleno.
		{
			Main.bloquearProceso_C();
			Main.desbloquearProceso_P();
		}
		mutex.P(); // asegura la exclusión mutua.
		buffer[inBuf] = value;
		inBuf = (inBuf+ 1) % size;
		mutex.V();
		isEmpty.V(); // notifica a algún consumidor en espera.
	}
	
	public double fetch()
	{
		double value;
		if(isEmpty.P()) // esperar si el buffer está vacío.
		{
			Main.bloquearProceso_P();
			Main.desbloquearProceso_C();
		}
		mutex.P(); // asegura la exclusión mutua.
		value = buffer[outBuf]; // lee desde el buffer.
		outBuf= (outBuf+1) % size;
		mutex.V();
		isFull.V(); // notifica a cualquier productor en espera.
		return value;
	}
	
}
